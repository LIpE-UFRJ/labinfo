import os
# from .libs.xmltodict import xmltodict
import xmltodict

## Obtém os dados do hardware:

# Entradas: origem -> lista/dicionário contendo listas/dicionários
#           rota -> lista contendo chaves (str) dos dicionários e pares
#                   chave-valor em forma de tuplas, para desambiguar entre
#                   várias chaves repetidas.
#                   Ex.: ["key1", "key2", ("key3", "value3"), key4]
#
# Saída: retorna o tipo de dado encontrado no elemento do dicionário
#
# Descrição: Função para achar um certo dado em uma estrutura do tipo gerado
# pelo método xmltodict.parse(). Tenta seguir a "rota", partindo do elemento
# "origem". Recursivamente, segue a árvore em forma de dicionário dada em "origem",
# obtendo o conteúdo das chaves que estão listadas em "rota". Quando encontra
# uma lista como coteúdo uma no dicionário, obtém-se o elemento da lista que tem
# um par chave-valor concordado com o passado na lista "rota" na forma
# "chave=valor".
def getData (origem, rota):
	node = origem
	index = 0

	for index in range(0, len(rota)):
		if (isinstance(node, dict)):
			node = node[rota[index]]
		elif (isinstance(node, list)):
			for i in range (0, len(node)):
				element = node[i]
				if (rota[index][0] in element.keys()):
					if (element[rota[index][0]] == rota[index][1]):
						node = element
						break
				if (i == len(node)):
					# Exceção.
					print ("Nenhum elemento do nó corresponde ao par chave-valor indicado: " + rota[index])
		else:
			# Exceção.
			print ("O tipo deste elemento não é lista ou dicionário.")

	return node

class Computer ():
	# Constantes
	__labinfoFileName = "labinfo.xml"
	__lshwFileName = "lshw.xml"
	__lsblkTxtFileName = "lsblk.txt"
	__lsblkJsonFileName = "lsblk.xml"
	__dmesgFileName = "dmesg.txt"

	# Dicionário para armazenar os dados do Labinfo.
	# Inicialização necessária para evitar erros de acesso a chaves inexistentes no dicionário.
	labinfo = {
		"Hardware" : {} ,
		"Health" : {} ,
		"Software" : {}
	}

	def __init__ (self, parentFolder, code):
		self.code = code                  # Código identificador do computador.
		self.path = parentFolder + code   # Diretório onde de dados para este computador.
		self.lshw = {}                    # Dicionário com os dados do lshw.
		self.lsblk = {}                   # Dicionário com os dados do lsblk.

	def run (self, command, outputFileName):
		commandStatus = os.system(f"{command} > '{self.path}/{outputFileName}' 2>&-")
		if (commandStatus != 0):
			print (f"Erro ao executar '{command}': {str(commandStatus)}")

	def getFiles (self, lshw=True, lsblk=True, dmesg=True):
		if (lshw == True):
			# Obtém informações de hardware pelo lshw:
			print ("Obtendo lshw... ")
			self.run ("lshw -xml", self.__lshwFileName)
		if (lsblk == True):
			# Obtém as informações sobre armazenamento pelo lsblk:
			print ("Obtendo lsblk... ")
			self.run ("lsblk", self.__lsblkTxtFileName)
			self.run ("lsblk --json --bytes", self.__lsblkJsonFileName)
		if (dmesg == True):
			# Obtém o log do kernel pelo dmesg:
			print ("Obtendo o log do kernel... ")
			if (os.geteuid() == 0):
				self.run ("dmesg", self.__dmesgFileName)
			else:
				print ("Falha ao obter o log do Kernel: necessita de privilégios de super-usuário.")

		os.system(f"chmod -f 666 {self.path}/{self.__labinfoFileName}")

	def getDataFromFiles (self):
		# Extrai a informação do lshw no arquivo para um atributo da classe.
		file = open(f"{self.path}/{self.__lshwFileName}", 'r')
		containing = file.read()
		file.close()
		self.lshw = xmltodict.parse(containing) # lshw em forma de dicionário!!

		# Extrai a informação do lsblk no arquivo para um atributo da classe.
		file = open(f"{self.path}/{self.__lsblkJsonFileName}", 'r')
		containing = file.read()
		file.close()
		false = False
		true = True
		null = None
		self.lsblk = eval(containing) # lsblk em forma de dicionário!!

	def getProcessedHardwareData (self):
		# Inicializa elementos do dicionário para evitar erros de acesso a chaves inexistentes.
		self.labinfo ["Hardware"] = {
			"Tipo" : "" ,
			"Placa-mae" : {
				"Modelo" : "" ,
				"Fabricante" : ""
			} ,
			"CPU" : {
				"Modelo" : "" ,
				"Arquitetura" : "" ,
				"Frequencia" : "" ,
				"Nucleos" : "" ,
				"Threads" : ""
			} ,
			"RAM" : {
				"Total" : {
					"Tamanho" : "" ,
					"Tecnologia" : "" ,
					"Frequencia_de_operacao" : "" ,
					"Modo_de_operacao" : ""
				}
			} ,
			"Armazenamento" : {} ,
			"Drive_optico" : {}
		}

		# Obtém os dados sobre o hardware.
		self.labinfo["Hardware"]["Tipo"] = getData(self.lshw, ["list", "node", "description"])

		self.labinfo["Hardware"]["Placa-mae"]["Modelo"] = getData(self.lshw, ["list","node","node",("@id","core"),"node",("@id","board"),"product"])
		self.labinfo["Hardware"]["Placa-mae"]["Fabricante"] = getData(self.lshw, ["list","node","node",("@id","core"),"node",("@id","board"),"vendor"])

		self.labinfo["Hardware"]["CPU"]["Modelo"] = getData(self.lshw, ["list","node","node",("@id","core"),"node",("@id","cpu"),"product"])
		self.labinfo["Hardware"]["CPU"]["Arquitetura"] = getData(self.lshw, ["list","node","node",("@id","core"),"node",("@id","cpu"),"width","#text"])
		self.labinfo["Hardware"]["CPU"]["Frequencia"] = getData(self.lshw, ["list","node","node",("@id","core"),"node",("@id","cpu"),"clock","#text"])
		self.labinfo["Hardware"]["CPU"]["Nucleos"] = getData(self.lshw, ["list","node","node",("@id","core"),"node",("@id","cpu"),"configuration","setting",("@id","cores"),"@value"])
		self.labinfo["Hardware"]["CPU"]["Threads"] = getData(self.lshw, ["list","node","node",("@id","core"),"node",("@id","cpu"),"configuration","setting",("@id","threads"),"@value"])

		self.labinfo["Hardware"]["RAM"]["Total"]["Tamanho"] = getData(self.lshw, ["list","node","node",("@id","core"),"node",("@id","memory"),"size","#text"])

	def getProcessedHealthData (self):
		# Inicializa elementos do dicionário para evitar erros de acesso a chaves inexistentes.
		### COMPLETAR CÓDIGO ###

		# Obtém os dados sobre o estado do hardware.
		### COMPLETAR CÓDIGO ###

		pass

	def getProcessedSoftwareData (self):
		# Inicializa elementos do dicionário para evitar erros de acesso a chaves inexistentes.
		### COMPLETAR CÓDIGO ###

		# Obtém os dados sobre softwares instalados no computador.
		### COMPLETAR CÓDIGO ###

		pass

	def getProcessedData (self):
		self.getProcessedHardwareData()
		self.getProcessedHealthData()
		self.getProcessedSoftwareData()

	def saveData (self):
		labinfoXml = xmltodict.unparse({"root" : self.labinfo}, encoding='utf-8', pretty=True)
		print ("Gravando informações em arquivo... ")
		try:
			labinfoFile = open(f"{self.path}/{self.__labinfoFileName}", 'w')
			labinfoFile.write(labinfoXml + "\n")
			labinfoFile.close()
			os.system(f"chmod -f 666 {self.path}/{self.__labinfoFileName}")
		except Exception as e:
			print ("Erro: " + str(e))
