#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Programa para extração de informações sobre hardware e software de um computador.
# Necessita de privilégios de super-usuário!
# Use python 3.x


import os
import re
from datetime import datetime
# from .libs.xmltodict import xmltodict
import xmltodict
from modules.computer import Computer

# Extrai a informação do sistema, gravando-a em arquivos
def extractFromSystem (dataPath):

	# Verifica se está com privilégios de super-usuário:
	###	Ver se é possível pedir a senha e obter os privilégios aqui.
	### Dar boas práticas de programação no uso das cores e aplicar às outras mensagens do programa.
	if (os.geteuid() != 0):
		print ("\033[1m" + "AVISO: " + "\033[31m" + "você deveria executar este programa como super-usuário." + "\033[0m")
		print ("       Sem privilégios de super-usuário algumas informações não podem ser acessadas.")
		print ("")
		print ("\033[1m" + "USO: " + "\033[0m" + "sudo python3 extrator.py")
		print ("\033[1m" + " OU: " + "\033[0m" + "su")
		print ("     python3 extrator.py")
		print ("")

	# Solicita dados ao usuário:

	## Confirma data e hora atuais:

	now = datetime.now()

	answer = input ("A informação de data e hora do sistema (" + now.strftime("%d/%m/%Y %H:%M") + ") está correta? (S/N) ")

	if not (answer.upper() == "S"):
		print ("")
		print ("A data e a hora são importantes para o registro.")
		print ("Acerte-as primeiro e rode o programa novamente.")
		exit ()

	## Solicita a pasta do laboratório onde registrar o computador:

	print ("Agora informe a pasta onde registrar o computador.")
	print ("Por padrão, o computador será registrado em uma pasta 'Labinfo - dados', ao lado da pasta deste programa.")
	print ("Dica: use a mesma pasta para os computadores de um mesmo laboratório.")

	### Imprime as pastas já existentes dentro do diretório padrão informado:

	if not (os.path.exists(dataPath)):
		os.mkdir(dataPath)

	entrieList = os.listdir(dataPath)  # listagem do diretório.

	folderList = []
	for entrie in entrieList:
		if (os.path.isdir(dataPath + entrie)):
			folderList += [entrie]

	if (folderList != []):
		print (f"OBS.: Já existem pastas dentro da '{dataPath}':")
		for folderName in folderList:
			print (folderName)

	### Finalmente, solicita o nome da pasta ao usuário e a cria se preciso:

	labFolder = input ("Nome da pasta: ")  ### Encontrar uma forma de ter um autocompletar (tecla TAB) relativo à pasta padrão do registro e não à pasta do programa.

	if (labFolder[0] != "/"):
		labFolder = dataPath + labFolder + "/"

	if not (os.path.exists(labFolder)):
		os.mkdir(labFolder)

	re_computerCode = re.compile (r'^\d\d\.\d\d\.\d\d\d$')
	computerCode = input ("Digite o código deste computador (formato XX.XX.XXX): ")
	while not (re_computerCode.match(computerCode)):
		print ("Formato do código incorreto.")
		computerCode = input ("Tente novamente (formato XX.XX.XXX): ")

	computerFolder = labFolder + computerCode + "/"

	if not (os.path.exists(computerFolder)):
		os.mkdir(computerFolder)

	thisComputer = Computer(labFolder, computerCode)
	thisComputer.getFiles()
	thisComputer.getDataFromFiles()
	thisComputer.getProcessedData()
	thisComputer.saveData()
	os.system(f"chmod -R 666 '{dataPath}'")
