#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Imports da biblioteca padrão
import argparse
import json
import os
import sys

# Imports locais
import modules.extrator as extractor

# Definições padrão de variáveis.
program_dir = os.path.dirname(os.path.abspath(__file__))

help_description='''\
Um programa opensource pra obtenção e registro de especificações de computadores em um contexto de múltiplos laboratórios de informática.'''
help_epilog='''\
Para saber mais, acesse o repositório de código fonte.
    Código-fonte: https://gitlab.com/LIpE-UFRJ/labinfo'''

# Pega opções de linha de comando com dados sobre o local do projeto e etc.
parser = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter, 
    description=help_description, 
    epilog=help_epilog
    )

parser.add_argument('-e', '--extract', metavar='FOLDER', nargs='?', default=None, const='../Labinfo - Dados/', help='Extrai os dados do computador e os organiza na pasta de destino indicada (Padrão: "../Labinfo - Dados").')
parser.parse_args()

# Executa as operações do programa.
if (parser.parse_args().extract != None):
    extractor.extractFromSystem( parser.parse_args().extract )
